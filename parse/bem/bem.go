package bem

import (
	"regexp"
	"strings"

	"gitlab.com/go-mods/bin/bemtab/table"
)

// Underscores ('_') are prohibited (except for when defining elements)
const name_exp = `([a-zA-Z0-9]+|([a-zA-Z0-9]*-[a-zA-Z0-9]+)+)`
const desc_exp = `(/\*\s*(([^\*]|\*+[^\*/])*?)\s*\*/)?`
const name_index, desc_index = 4, 2

func ParseTable(css string) *table.Table {
	// Find blocks
	block_regx := regexp.MustCompile(desc_exp + `\s*\.` + name_exp + `[\s{]`)
	blocks := block_regx.FindAllStringSubmatch(css, -1)

	// Create block (full) table
	block_rows := []*table.TableRow{table.CreateRow(
		table.ParseCell("Block"),
		table.ParseCell("Description"),
		table.ParseCell("Elements"),
		table.ParseCell("Modifiers"))}

	for i, len_blocks := 0, len(blocks); i < len_blocks; i++ {
		block_name := blocks[i][name_index]
		block_desc := strings.ReplaceAll(blocks[i][desc_index], "\n", " ")

		// Find block elements
		elemt_regexp := regexp.MustCompile(desc_exp + `\s*\.` + block_name + `__` + name_exp + `[\s{]`)
		block_elemts := elemt_regexp.FindAllStringSubmatch(css, -1)

		// Create element (sub) table
		block_elemts_rows := []*table.TableRow{table.CreateRow(
			table.ParseCell("Element"),
			table.ParseCell("Description"),
			table.ParseCell("Modifiers"))}

		for j, len_elemts := 0, len(block_elemts); j < len_elemts; j++ {
			elemt_name := block_elemts[j][name_index]
			elemt_desc := strings.ReplaceAll(block_elemts[j][desc_index], "\n", " ")

			// Find element modifiers
			modif_regexp := regexp.MustCompile(desc_exp + `\s*\.` + block_name + `__` + elemt_name + `--` + name_exp + `[\s{]`)
			elemt_modifs := modif_regexp.FindAllStringSubmatch(css, -1)

			// Create modifier (sub sub) table
			elemt_modifs_rows := []*table.TableRow{table.CreateRow(
				table.ParseCell("Modifier"),
				table.ParseCell("Description"))}

			for k, len_modifs := 0, len(elemt_modifs); k < len_modifs; k++ {
				modif_name := elemt_modifs[k][name_index]
				modif_desc := strings.ReplaceAll(elemt_modifs[k][desc_index], "\n", " ")

				// Add row to element modifiers table
				elemt_modifs_rows = append(elemt_modifs_rows, table.CreateRow(
					table.ParseCell(modif_name),
					table.ParseCell(modif_desc)))
			}

			// Add row to block elements table
			block_elemts_rows = append(block_elemts_rows, table.CreateRow(
				table.ParseCell(elemt_name),
				table.ParseCell(elemt_desc),
				table.CreateCell(table.CreateTable(elemt_modifs_rows...))))
		}

		// Find block modifiers
		modif_regexp := regexp.MustCompile(desc_exp + `\s*\.` + block_name + `--` + name_exp + `[\s{]`)
		block_modifs := modif_regexp.FindAllStringSubmatch(css, -1)

		// Create modifier (sub sub) table
		block_modifs_rows := []*table.TableRow{table.CreateRow(
			table.ParseCell("Modifier"),
			table.ParseCell("Description"))}

		for j, len_modifs := 0, len(block_modifs); j < len_modifs; j++ {
			modif_name := block_modifs[j][name_index]
			modif_desc := strings.ReplaceAll(block_modifs[j][desc_index], "\n", " ")

			// Add row to block modifiers table
			block_modifs_rows = append(block_modifs_rows, table.CreateRow(
				table.ParseCell(modif_name),
				table.ParseCell(modif_desc)))
		}

		// Add row to block table
		block_rows = append(block_rows, table.CreateRow(
			table.ParseCell(block_name),
			table.ParseCell(block_desc),
			table.CreateCell(table.CreateTable(block_elemts_rows...)),
			table.CreateCell(table.CreateTable(block_modifs_rows...))))
	}

	return table.CreateTable(block_rows...)
}
