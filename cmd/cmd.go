package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/go-mods/bin/bemtab/parse/bem"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sb := strings.Builder{}

	for sc.Scan() {
		sb.WriteString(sc.Text() + "\n")
	}

	if err := sc.Err(); err != nil {
		fmt.Fprintf(os.Stderr, "pipe error: %s\n", err)
	}

	fmt.Println(bem.ParseTable(sb.String()).Html())
}
