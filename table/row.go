package table

import (
	"fmt"
	"strings"
)

// The TableRow type describes a single row within a table.
// TableRow* implements the Tabable interface.
type TableRow struct {
	cells []*TableCell
}

// Creates a new TableRow with the given cells.
func CreateRow(cells ...*TableCell) *TableRow {
	return &TableRow{cells}
}

// Parses a given string into a TableRow.
func ParseRow(str string) *TableRow {
	panic("not implemented")
}

// Implementation of Tabable.Html().
func (tr *TableRow) Html() string {
	sb := strings.Builder{}
	sb.WriteString("<tr>\n")

	for i, l := 0, len(tr.cells); i < l; i++ {
		sb.WriteString(indent(fmt.Sprintf(
			"%s", tr.cells[i].Html()),
			TableIndent) + "\n")
	}

	sb.WriteString("</tr>")
	return sb.String()
}
