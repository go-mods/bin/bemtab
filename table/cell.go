package table

import "strings"

// The TableCell type describes a single cell within a table.
// TableCell* implements the Tabable interface.
type TableCell struct {
	html string
}

// Creates a new TableCell with the given content.
func CreateCell(content Tabable) *TableCell {
	return &TableCell{content.Html()}
}

// Parses a given string into a TableCell.
func ParseCell(str string) *TableCell {
	return &TableCell{str} // todo: validation etc.
}

// Implementation of Tabable.Html().
func (tc *TableCell) Html() string {
	sb := strings.Builder{}
	sb.WriteString("<td>\n")
	sb.WriteString(indent(tc.html, TableIndent) + "\n")
	sb.WriteString("</td>")
	return sb.String()
}
