package table

import "strings"

// Adds the given prefix to each line of the string and discards
// blank lines.
func indent(str string, pre string) string {
	lines := strings.Split(str, "\n")
	sb := strings.Builder{}

	for i, l := 0, len(lines); i < l; i++ {
		s := lines[i]

		if len(strings.TrimLeft(s, " \t\r\n")) > 0 {
			sb.WriteString(pre + s)

			if i < l-1 {
				sb.WriteString("\n")
			}
		}
	}

	return sb.String()
}
