package table

import (
	"strings"
)

// The table type describes a 2-dimensional table.
type Table struct {
	// Wether the first row of a table is interpreted as header or not
	// (default true).
	HasHeader bool
	rows      []*TableRow
}

// Creates a new Table with the given rows.
func CreateTable(rows ...*TableRow) *Table {
	return &Table{true, rows}
}

// Parses a given string into a Table.
func ParseTable(str string) *Table {
	panic("not implemented")
}

// Implementation of Tabable.Html().
func (tb *Table) Html() string {
	sb := strings.Builder{}
	sb.WriteString("<table>\n")
	var start int

	if tb.HasHeader {
		if len(tb.rows) > 1 {
			sb.WriteString(indent("<thead>", TableIndent) + "\n")
			sb.WriteString(indent(strings.ReplaceAll(tb.rows[0].Html(), "td>", "th>"), TableIndent+TableIndent) + "\n")
			sb.WriteString(indent("</thead>", TableIndent) + "\n")
		} else {
			return ""
		}

		start = 1
	} else {
		start = 0
	}

	if l := len(tb.rows); start < l {
		sb.WriteString(indent("<tbody>", TableIndent) + "\n")

		for i := start; i < l; i++ {
			sb.WriteString(indent(tb.rows[i].Html(), TableIndent+TableIndent) + "\n")
		}

		sb.WriteString(indent("</tbody>", TableIndent) + "\n")
	} else {
		return ""
	}

	sb.WriteString("</table>")
	return sb.String()
}
