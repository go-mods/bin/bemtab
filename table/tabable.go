package table

// Indentation used for formatted output of table elements.
var TableIndent = "  "

// The Tabable interface declares functions to retrieve tables and
// table contents in various formats as strings.
type Tabable interface {
	Html() string
}
