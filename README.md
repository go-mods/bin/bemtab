# BEMTab

A small cli tool to generate HTML tables, for documentation purposes, out of CSS following BEM principles.

## Installation

Building from source requires [Go 1.17](https://go.dev/) or higher. Pre build binaries can be downloaded [here](https://gitlab.com/go-mods/bin/bemtab/-/releases).

## Example

Given following CSS stored in `example.css`:

```css
/* Block 1 description */
.block {
    /* very flexible */
    display: flex;
}
/* Another Block */
.block-2 {}
/* Block Element 1 */
.block__elem {}
/* Block Element 3 */
.block__elem-x-y {}
/*
 This modifier is for
 Element 1.
*/
.block__elem--mod-1 {}
.block__elem--mod-2 {}
/* Blocks can have modifiers too */
.block-2--mod {}
```

The call `cat example.css | bemtab.exe` outputs following HTML-Table:

<table>
  <thead>
    <tr>
      <th>
        Block
      </th>
      <th>
        Description
      </th>
      <th>
        Elements
      </th>
      <th>
        Modifiers
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        block
      </td>
      <td>
        Block 1 description
      </td>
      <td>
        <table>
          <thead>
            <tr>
              <th>
                Element
              </th>
              <th>
                Description
              </th>
              <th>
                Modifiers
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                elem
              </td>
              <td>
                Block Element 1
              </td>
              <td>
                <table>
                  <thead>
                    <tr>
                      <th>
                        Modifier
                      </th>
                      <th>
                        Description
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        mod-1
                      </td>
                      <td>
                        This modifier is for  Element 1.
                      </td>
                    </tr>
                    <tr>
                      <td>
                        mod-2
                      </td>
                      <td>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                elem-x-y
              </td>
              <td>
                Block Element 3
              </td>
              <td>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
      </td>
    </tr>
    <tr>
      <td>
        block-2
      </td>
      <td>
        Another Block
      </td>
      <td>
      </td>
      <td>
        <table>
          <thead>
            <tr>
              <th>
                Modifier
              </th>
              <th>
                Description
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                mod
              </td>
              <td>
                Blocks can have modifiers too
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>

<details>
<summary>Click to show source.</summary>

```
<table>
  <thead>
    <tr>
      <th>
        Block
      </th>
      <th>
        Description
      </th>
      <th>
        Elements
      </th>
      <th>
        Modifiers
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        block
      </td>
      <td>
        Block 1 description
      </td>
      <td>
        <table>
          <thead>
            <tr>
              <th>
                Element
              </th>
              <th>
                Description
              </th>
              <th>
                Modifiers
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                elem
              </td>
              <td>
                Block Element 1
              </td>
              <td>
                <table>
                  <thead>
                    <tr>
                      <th>
                        Modifier
                      </th>
                      <th>
                        Description
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        mod-1
                      </td>
                      <td>
                        This modifier is for  Element 1.
                      </td>
                    </tr>
                    <tr>
                      <td>
                        mod-2
                      </td>
                      <td>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                elem-x-y
              </td>
              <td>
                Block Element 3
              </td>
              <td>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
      </td>
    </tr>
    <tr>
      <td>
        block-2
      </td>
      <td>
        Another Block
      </td>
      <td>
      </td>
      <td>
        <table>
          <thead>
            <tr>
              <th>
                Modifier
              </th>
              <th>
                Description
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                mod
              </td>
              <td>
                Blocks can have modifiers too
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
```

</details>

---

## Disclaimer

Due to technical circumstances a strict convention for naming CSS-classes must be followed:

1. Blocks, Elements and Modifiers may consist only of alphanumerical characters which may be separated by single hyphens '-'.
2. Blocks and Elements are seperated by double underscores '__', Modifiers are seperated by double hyphens '--'.
